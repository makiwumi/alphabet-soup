/**
 * Alphabet Soup Challenge
 * Marian Akiwumi
 * 6/4/2024
 **/

// Use file system module
const fs = require('fs');

// Parse file to breakdown text
function parseFile() {
    //Read file text
    const text = fs.readFileSync('input.txt', 'utf-8')
    
    // Separate text by lines to create an array
    const lines = text.split('\n').map(line => line.trim());

    // Find grid size from first line (a x b) in array to get number of rows
    const gridSize = lines[0].split('x').map(Number)
    const numRows = gridSize[0]
    
    // Split each row into an array & separate the characaters to get a grid
    const grid = lines.slice(1, numRows + 1).map(row => row.trim().split(/\s+/));
    
    // Pull words from last remaining lines
    const words = lines.slice(numRows + 1)
  
    return [grid, words]
    
}

//Search for words in the grid
function wordSearch(grid, word) {
    // Define number of rows and cols
    const rows = grid.length;
    const cols = grid[0].length;


    // Find location of the first letter in each word
    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            // Define different directions for the rest of the word
            for (let [rowDir, colDir] of [[1, 0], [0, 1], [1, 1], [-1, 0], [0, -1], [-1, -1], [1, -1], [-1, 1]]) {
                // Check to see if word matches based on direction
                let match = true;
                
                for (let i = 0; i < word.length; i++) {
                    const r = row + i * rowDir;
                    const c = col + i * colDir;
                    
                    if (r < 0 || r >= rows || c < 0 || c >= cols || grid[r][c] !== word[i]) {
                        match = false;
                    }
                }

                // If match is true, return indices for the first and last character of each word as a string
                if (match) {
                    const first = `${row}:${col}`;
                    const last = `${row + (word.length - 1) * rowDir}:${col + (word.length - 1) * colDir}`;
                    return `${word} ${first} ${last}`;
                }
            }
        }
    }
    return `${word} not found!`;
}

// Define grid and words from parsed file ouside the function
const [grid, words] = parseFile();

// Pull each word from the words array
for (let word of words) {
    //Run search function for each word pulled
    const result = wordSearch(grid, word);
    console.log(result);
}




